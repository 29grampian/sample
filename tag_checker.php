<?php
/**
 * 
 * OVGuide generated income from affiliate reveune. 
 * 
 * When you landed on a movie page at OVGuide and Amazon was selling that streaming movie title, we displayed a button that linked to the Amazon page. 
 * 
 * If you clicked to Amazon and purchased the movie, bought some other items or signed up for Prime membership, we received a commission. The same applied to other partners such as HBOGO, VUDU(walmart), HULU, etc.
 * 
  * Frequently we bought ads that linked back to our pages. We bought ads from different sources: Bing, Adwords, Facebook etc. 
  * 
  * For example, a new tv show is available on Amazon Prime UK, we would buy Facebook and Bing ads in UK that linked back to corresponding TV page.
 * 
 * We needed a way to calculate the ROI to see if Facebook or Bing generated better conversion and income. Sometimes we wished to find out more which device performed better.
 * 
 * We checked the condition of the request and generated unique Tracking IDs. For example, in the Amazon dashboard, if we see tracking_id=campaign_facebook_uk_ipad was doing better, we allocated our budget accordingly. These test cycles were 1 week max.
 * 
 * We needed a way to quickly add new rules without touching the core code. The rules varied and we found it more convenient to keep them in a array.
 * 
 * Below is a summary of the code I wrote.
 * 
 * 1. Rules - an array of rules. Kept separate from the logic. We had a few dozens rules max. We use the key as rank.
 * 2. Checkers - individual checkers sharing one interface. We added more checkers as the business needs changed. In the end we had about 10.
 * 3. Black box - One closed core that run the checkers. It rarely needed changes. It depends on abstraction.
 * 4. Inovcation code - we called the black box on a few page types.
 * 
 * Goal
 * 1. A closed core with open rule sets so any developers can add new rules to one file one array without touching the core.
 * 2. One point of coupling between condition and checker.
 * 2. A standard checker interface so we could add new checker easily as our needs changed.
 * 3. Use DI to inject checkers interface into the blackbox. The blackbox doesn't have to know what it is checking.
 */ 

/**
 * Rules that match partner and conditions to tag. Key is used as ranking.
 */ 
$rules_partner_condition_tag = array(
	//partner
	'amazon'=> array(
		//rule 1
		1 => array(
			'conditions' => array(
				array(
					'value' => 'ipad',
					'checker' => 'tagCheckerDevice'
				),
				array(
					'value'=>'bing',
					'checker'=> 'tagCheckerUtmSource'
				),
				array(
					'value'=>'UK',
					'checker'=> 'tagCheckerGeo'
				)
			),
			'tracking_id' =>'campaign_bing_uk_ipad',
		),
		//rule 2
		2 => array(
			'conditions' => array(
				array(
					'value' => 'ipad',
					'checker' => 'tagCheckerDevice'
				),
				array(
					'value'=>'facebook',
					'checker'=>'tagCheckerUtmSource'
				),
				array(
					'value'=>'UK',
					'checker'=>'tagCheckerGeo'
				)
			),
			'tracking_id' =>'campaign_facebook_uk_ipad',
		),
		
		//rule 3
		3 => array(
			'conditions' => array(
				array(
					'value' => '8fda35e4aa3840d292b5ab702d10367e',
					'checker' => 'tagCheckerItemGuid'
				),
			),
			'tracking_id' =>'campaign_for_a_specific_movie',
		),
		//rule N / default
		10000 => array(
			'tracking_id'=>'default_amazon_tag'
		),
	),
	'hbo' => array(
		//rule 1 for hbo
		1 => array(
			//nothing to match, this is a default rule
			'tracking_id' => 'default_hbo_tag'
		)
	),
	// 'cbs'=>array(
	 	//another partner
	// ),
	//more partners to follow...
);

/**
 * Interface for all tag checkers.
 */
interface tagCheckerInterface{
	public function check($arg);
}

/**
 * Checker - utm_source.
 * Check for Urchin Traffic Monitor (UTM) source. utm_source is used in ad buying world to identify where you are buying ad from. i.e. You buy $100 of adds on facebook, and you link to your page like so: https://www.mydomain.com/item_guid=8fda35e4aa3840d292b5ab702d10367e?utm_source=facebook...
 *  
 * @implements tagCheckerInterface
 */
class tagCheckerUtmSource implements tagCheckerInterface{
	/**
	 * @param string $arg 
	 * @return bool
	 */
	public function check($arg){
		$current_utm = 'facebook'; // call code that get the utm_source from url i.e. 
		return ($arg === $current_utm);
	}
}

/**
 * Checker - item GUID
 * We have a condition for GUID xxxxx, get the current page item guid and see if it matches a condition
 * 
 * @implements tagCheckerInterface
 */
class tagCheckerItemGUID implements tagCheckerInterface{
	/**
	 * @param string $arg 
	 * @return bool
	 */
	public function check($arg){
		$current_item_id = '8fda35e4aa3840d292b5ab702d10367e'; //call code that keep track of current state and return the current page item GUID
		return ($arg === $current_item_id);
	}
}

/**
 * Checker - Device checker
 * i.e. We have condition for device 'ipad'. We use a mobile detect library to check the device the client is currently on.
 * 
 * @implements tagCheckerInterface
 */
class tagCheckerDevice implements tagCheckerInterface{
	/**
	 * @param string $arg 
	 * @return bool
	 */
	public function check($arg){
		$current_device = 'ipad'; //call some mobile detect library to get current device...
		return ($arg === $current_device);
	}
}

/**
 * Checker - Geo / country code checker
 * We have a condition looking for country UK. Let's see what country the current user is in.
 * 
 * @implements tagCheckerInterface
 */
class tagCheckerGeo implements tagCheckerInterface{
	/**
	 * @param string $arg 
	 * @return bool
	 */
	public function check($arg){
		$currentCountryUserIsIn = 'UK'; //call some geo detect library to get user current country code...
		return ($arg === $currentCountryUserIsIn);
	}
}

//more class tagChecker such as tagCheckerIsMovie, tagCheckerIsTV


/**
 * Black box checker. This is the core / closed logic that calculate the correct tag.
 * Once the code is stablized, the blackbox code rarely needs changing.
 */
class tagCheckerBlackbox{
	
	private $checkers = array();
	
	/**
	 * Attach checker objects to checkers array
	 * @param tagCheckerInterface $checker 
	 * 
	 */
	public function attach(tagCheckerInterface $checker){
		$classname = get_class($checker);
		$this->checkers[$classname] = $checker;
	}
	
	/**
	 * Main code to calculate the correct tag based on condition. and adjust the video url by reference
	 * @param array $rules 
	 * @param array &$links passed by reference, adjust the video url 
	 * 
	 */
	public function adjust($rules, &$affiliate_links){

		foreach($affiliate_links as $key=>$value){
			
			if(array_key_exists($value['vendor'], $rules)){
			
				//i.e. vendor is amazon, we pick out $this->rules['amazon'] as $rulesActive... 
				$rulesActive = $rules[$value['vendor']];
				
				//sort by key. Key is used as rank
				ksort($rulesActive);
				
				foreach($rulesActive as $oneRule){
					if(!empty($oneRule['conditions'])){
						//do we have conditions to match?
						$matchedCondition = 0; //for each rule, starts with 0 matched condition
						$toMatchedCondition = count($oneRule['conditions']);
					
						foreach($oneRule['conditions'] as $oneCond){
						
							if(array_key_exists($oneCond['checker'], $this->checkers)){
								//i.e. this condition wants to use tagCheckerDevice, grab the right checkers from $this->checkers['tagCheckerDevice']
								if($this->checkers[$oneCond['checker']]->check($oneCond['value'])){
									$matchedCondition++;
								}
							}
						}
						//done looping the conditions...
						if($matchedCondition === $toMatchedCondition){
							//i.e. the rule sets specified 3 conditions to match, we have matched 3, we have a tracking_id to use
							$trackingIDFinal = $oneRule['tracking_id'];
							break;
						}
					}else{
						//No conditions to match, use default
						$trackingIDFinal = $oneRule['tracking_id'];
					}
				}
				$affiliate_links[$key]['url'] = $this->append_tracking_id_to_url($trackingIDFinal,  $affiliate_links[$key]['url']);
			}

		}
	}
	private function append_tracking_id_to_url(string $id, string $url): string{
		return $url . '&tracking_id=' . $id; 
	}
}

/**
 * When a page loaded, we knew this movie or tv title has these availabe_affiliate_links
 */ 
$affiliate_links = array(
	array('vendor'=>'amazon', 'url'=>'https://www.amazon.com/?title=westworld&item_id=123'),
	array('vendor'=>'hbo', 'url'=>'https://www.hbo.com/?title=westworld&item_id=456')
);

//Invocation
$myTagChecker = new tagCheckerBlackbox();

$myTagChecker->attach(new tagCheckerUtmSource());
$myTagChecker->attach(new tagCheckerDevice());
$myTagChecker->attach(new tagCheckerItemGUID());
$myTagChecker->attach(new tagCheckerGeo());

$myTagChecker->adjust($rules_partner_condition_tag, $affiliate_links);

print_r($affiliate_links);
/*
Returns
Array
(
    [0] => Array
        (
            [vendor] => amazon
            [url] => https://www.amazon.com/?title=westworld&item_id=123&tracking_id=campaign_facebook_uk_ipad
        )

    [1] => Array
        (
            [vendor] => hbo
            [url] => https://www.hbo.com/?title=westworld&item_id=456&tracking_id=default_hbo_tag
        )

)
*/